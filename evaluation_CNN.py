#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 17:39:49 2018

@author: anutza
"""

from keras.models import load_model
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator

img_size = 128
batch_size = 10

model=load_model('model_fruits.h5')
test = ImageDataGenerator(rescale=1./255,horizontal_flip=True, shear_range=0.2, zoom_range=0.2,width_shift_range=0.2,height_shift_range=0.2)
test_gen = test.flow_from_directory("/home/anutza/Documents/ASI$/Python/Projet/fruits-360/Test", target_size = (img_size, img_size), batch_size = batch_size, class_mode='categorical')

test_steps = 11848/batch_size

score = model.evaluate_generator(test_gen,steps=test_steps)
print("Accuracy = " + format(score[1]*100, '.2f') + "%")   