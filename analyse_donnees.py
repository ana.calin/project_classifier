#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 22 18:19:42 2018

@author: anutza
"""

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import matplotlib.pyplot as plt
import numpy as np
from os import listdir
from os.path import join
import cv2
import os
import random as rn

# Set the path of the input folder 

data = "flowers/"

# List out the directories inside the main input folder

folders = os.listdir(data)

print(folders)

# Import the images and resize them to a 128*128 size
# Also generate the corresponding labels

image_names = []
train_labels = []
train_images = []

size = 128,128

for folder in folders:
    for file in os.listdir(os.path.join(data,folder)):
        if file.endswith("jpg"):
            image_names.append(os.path.join(data,folder,file))
            train_labels.append(folder)
            img = cv2.imread(os.path.join(data,folder,file))
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            im = cv2.resize(img,size)
            train_images.append(im)
        else:
            continue

train = np.array(train_images)

train.shape



fig,ax=plt.subplots(5,2)
fig.set_size_inches(15,15)
for i in range(5):
    for j in range (2):
        l=rn.randint(0,len(train_labels))
        ax[i,j].imshow(train[l])
        ax[i,j].set_title('Flower: '+train_labels[l])

train = train.astype('float32') / 255.0
#%%
#%%
label_to_id = {v : k for k, v in enumerate(np.unique(train_label))}
id_to_label = {v : k for k, v in label_to_id.items()}
training_label_id = np.array([label_to_id[i] for i in train_label])
test_label_id = np.array([label_to_id[i] for i in test_label])
test_label_id
training_fruit_img, test_fruit_img = training_fruit_img / 255.0, test_fruit_img / 255.0 
plt.imshow(training_fruit_img[0])

#%%
model = keras.Sequential()
model.add(keras.layers.Conv2D(16, (3, 3), input_shape = (64, 64, 3), padding = "same", activation = "relu"))
model.add(keras.layers.MaxPooling2D(pool_size = (2, 2)))

model.add(keras.layers.Conv2D(32, (3, 3), padding = "same", activation = "relu"))
model.add(keras.layers.MaxPooling2D(pool_size = (2, 2)))

model.add(keras.layers.Conv2D(32, (3, 3), padding = "same", activation = "relu"))
model.add(keras.layers.MaxPooling2D(pool_size = (2, 2)))

model.add(keras.layers.Conv2D(64, (3, 3), padding = "same", activation = "relu"))
model.add(keras.layers.MaxPooling2D(pool_size = (2, 2)))

model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(256, activation = "relu"))
model.add(keras.layers.Dense(85, activation = "softmax"))
#%%
model.compile(loss = "sparse_categorical_crossentropy", optimizer = keras.optimizers.Adamax(), metrics = ['accuracy'])
tensorboard = keras.callbacks.TensorBoard(log_dir = "./Graph", histogram_freq = 0, write_graph = True, write_images = True)
model.fit(training_fruit_img, training_label_id, batch_size = 128, epochs = 10, callbacks = [tensorboard])
#%% shuffle

from sklearn.preprocessing import LabelEncoder
le=LabelEncoder()
trainY=le.fit_transform(train_labels)


shuffle = np.random.permutation(train.shape[0])
train_x=train[shuffle]
train_y=trainY[shuffle]

Z=[]
nxs=3025
nx, nlig, ncol, nprof = train.shape
train_x = train_x.reshape(nx,nlig*ncol*nprof)
#%% split

import sklearn.preprocessing as preprocessing
normalisateur = preprocessing.Normalizer().fit(train_x)
train_x = normalisateur.transform(train_x)

from sklearn.model_selection import StratifiedShuffleSplit

[(idx, dxs)] = StratifiedShuffleSplit(n_splits=1,train_size=nxs).split(train_x,train_y)

train_xC = train_x[idx]
train_yC = train_y[idx]


#%% pca
from sklearn.decomposition import PCA

pca = PCA(n_components=2).fit(train_xC)
X = pca.transform(train_xC)


import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt

plt.figure(1)
for c in [0,1,2,3,4]:
    idx, = np.nonzero(train_yC == c)
    plt.plot(X[idx,0],X[idx,1], '+' , label='%d' % (c,))
    

plt.legend()
plt.title('PCA')
plt.savefig('PCA.pdf')


#%% kmeans

from sklearn.cluster import KMeans

kmeans= KMeans(n_clusters=5)
kmeans.fit(train_xC)
y_kmeans=kmeans.predict(train_xC)

#%%
classe_0 = np.where(train_yC[:]==0)
classe_1 = np.where(train_yC[:]==1)
classe_2 = np.where(train_yC[:]==2)
classe_3 = np.where(train_yC[:]==3)
classe_4 = np.where(train_yC[:]==4)

classe_0_k = np.where(y_kmeans[:]==0)
classe_1_k = np.where(y_kmeans[:]==1)
classe_2_k = np.where(y_kmeans[:]==2)
classe_3_k = np.where(y_kmeans[:]==3)
classe_4_k = np.where(y_kmeans[:]==4)



#%%
err=0
for i in range(3024):
    if train_yC[i] == y_kmeans[i]:
        err=err
    else :
        err=err+1


