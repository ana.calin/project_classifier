#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 19:10:57 2018

@author: anutza
"""


import numpy as np
from keras import layers
from keras import models
from tensorflow.python.keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

train = ImageDataGenerator(rescale=1./255,horizontal_flip=True, shear_range=0.2, zoom_range=0.2,width_shift_range=0.2,height_shift_range=0.2, validation_split=0.3)

img_size = 128
batch_size = 10
t_steps = 24168/batch_size
v_steps = 10329/batch_size

train_gen = train.flow_from_directory("/home/anutza/Documents/ASI$/Python/Projet/fruits-360/Training", target_size = (img_size, img_size), batch_size = batch_size, class_mode='categorical', subset='training')
val_gen =  train.flow_from_directory("/home/anutza/Documents/ASI$/Python/Projet/fruits-360/Training", target_size = (img_size, img_size), batch_size = batch_size, class_mode='categorical', subset='validation')

#%%
model = models.Sequential()
model.add(layers.Conv2D(16, (3,3), activation='relu', input_shape=(img_size,img_size,3)))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Conv2D(32, (3,3), activation='relu'))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Conv2D(64, (3,3), activation='relu'))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Flatten())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dense(68, activation='softmax'))

model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])

model_hist = model.fit_generator(train_gen, steps_per_epoch=t_steps, epochs=2, validation_data=val_gen,validation_steps=v_steps)

model.save('model_fruits.h5')
#%%
acc = model_hist.history['acc']
val_acc = model_hist.history['val_acc']
loss = model_hist.history['loss']
val_loss = model_hist.history['val_loss']


plt.figure(figsize=(15, 6));
plt.subplot(1,2,1)
plt.plot( acc, color='#0984e3',marker='o',linestyle='none',label='Training Accuracy')
plt.plot( val_acc, color='#0984e3',label='Validation Accuracy')
plt.title('Training and Validation Accuracy')
plt.legend(loc='best')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')

plt.subplot(1,2,2)
plt.plot(loss, color='#eb4d4b', marker='o',linestyle='none',label='Training Loss')
plt.plot(val_loss, color='#eb4d4b',label='Validation Loss')
plt.title('Training and Validation Loss')
plt.legend(loc='best')
plt.xlabel('Epochs')
plt.ylabel('Loss')

plt.show()

#%%

